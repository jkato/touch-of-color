using System;

namespace TouchOfColor.Example
{
    class Program
    {
        static int Main(string[] args)
        {
            int exitCode;
            try
            {
                VirtualTerminal.Enable();
                exitCode = PerformTest();
            }
            finally
            {
                VirtualTerminal.Disable();
            }

            return exitCode;
        }

        static int PerformTest()
        {
            Test1();
            Test2();
            Test3();

            return 0;
        }

        static void Test1()
        {
            Console.WriteLine("Test #1:");
            for (int i = 0; i < 256; i++)
            {
                if (i % 32 == 0 && i > 0)
                {
                    Console.WriteLine();
                }

                Console.Write($"\x1b[48;2;255;{256 - i};0m \x1b[0m");
            }

            Console.WriteLine();
        }

        static void Test2()
        {
            Console.WriteLine("Test #2:");
            for (int i = 0; i < 256; i++)
            {
                if (i % 32 == 0 && i > 0)
                {
                    Console.WriteLine();
                }

                Console.Write($"\x1b[48;2;255;{i};0m \x1b[0m");
            }

            Console.WriteLine();
        }

        static void Test3()
        {
            Console.WriteLine("Test #3:");
            for (int i = 0; i < 256; i++)
            {
                if (i % 32 == 0 && i > 0)
                {
                    Console.WriteLine();
                }

                Console.Write($"\x1b[48;2;{i};{i};{i}m \x1b[0m");
            }

            Console.WriteLine();
        }
    }
}
