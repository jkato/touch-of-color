using System;
using System.ComponentModel;
using System.Runtime.InteropServices;

namespace TouchOfColor
{
    /// <summary>
    /// Provides an interface for interacting with virtual terminal modes in Windows.
    /// </summary>
    public class VirtualTerminal
    {
        private const string KERNEL32_DLL = "kernel32.dll";

        private const ulong INVALID_HANDLE_VALUE = 0xffffffffffffffff;
        private const uint STD_OUTPUT_HANDLE     = 0xfffffff5;
        private const uint STD_INPUT_HANDLE      = 0xfffffff6;

        private const uint ENABLE_VIRTUAL_TERMINAL_PROCESSING = 0x0004;
        private const uint ENABLE_VIRTUAL_TERMINAL_INPUT      = 0x0200;

        private enum VirtualTerminalMode
        {
            Disable = 0,
            Enable = 1,
        }

        [DllImport(KERNEL32_DLL, EntryPoint = "GetStdHandle")]
        private static extern IntPtr GetStdHandle(uint handle);

        [DllImport(KERNEL32_DLL, EntryPoint = "GetConsoleMode")]
        private static extern int GetConsoleMode(IntPtr handle, ref uint mode);

        [DllImport(KERNEL32_DLL, EntryPoint = "SetConsoleMode")]
        private static extern int SetConsoleMode(IntPtr handle, uint mode);

        public static bool Enable()
        {
            return SetVirtualTerminalMode(VirtualTerminalMode.Enable);
        }

        public static bool Disable()
        {
            return SetVirtualTerminalMode(VirtualTerminalMode.Disable);
        }

        private static bool SetVirtualTerminalMode(VirtualTerminalMode mode)
        {
            if (!RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                return true;
            }

            var hStdOut = GetStdOutHandle();
            var hStdIn = GetStdInHandle();

            uint outputMode = 0;
            uint inputMode = 0;

            GetConsoleMode(hStdOut, ref outputMode);
            GetConsoleMode(hStdIn, ref inputMode);

            switch (mode)
            {
                case VirtualTerminalMode.Enable:
                {
                    if ((outputMode & ENABLE_VIRTUAL_TERMINAL_PROCESSING) == 0)
                    {
                        outputMode |= ENABLE_VIRTUAL_TERMINAL_PROCESSING;
                        SetConsoleMode(hStdOut, outputMode);
                    }

                    if ((inputMode & ENABLE_VIRTUAL_TERMINAL_INPUT) == 0)
                    {
                        inputMode |= ENABLE_VIRTUAL_TERMINAL_INPUT;
                        SetConsoleMode(hStdIn, inputMode);
                    }

                    break;
                }
                case VirtualTerminalMode.Disable:
                {
                    if ((outputMode & ENABLE_VIRTUAL_TERMINAL_PROCESSING) != 0)
                    {
                        outputMode &= ~ENABLE_VIRTUAL_TERMINAL_PROCESSING;
                        SetConsoleMode(hStdOut, outputMode);
                    }

                    if ((inputMode & ENABLE_VIRTUAL_TERMINAL_INPUT) != 0)
                    {
                        inputMode &= ~ENABLE_VIRTUAL_TERMINAL_INPUT;
                        SetConsoleMode(hStdIn, inputMode);
                    }

                    break;
                    }
                default:
                    return false;
            }

            return true;
        }

        private static IntPtr GetStdOutHandle()
        {
            IntPtr hStdOut;
            try
            {
                hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
            }
            catch (Exception ex)
            {
                throw new VirtualTerminalException("Failed to acquire handle for stdout.", ex);
            }

            return hStdOut;
        }

        private static IntPtr GetStdInHandle()
        {
            IntPtr hStdIn;
            try
            {
                hStdIn = GetStdHandle(STD_INPUT_HANDLE);
            }
            catch (Exception ex)
            {
                throw new VirtualTerminalException("Failed to acquire handle for stdin.", ex);
            }

            return hStdIn;
        }

        private VirtualTerminal()
        { }
    }
}
