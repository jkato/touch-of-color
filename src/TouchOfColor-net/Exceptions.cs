using System;

namespace TouchOfColor
{
    public class VirtualTerminalException : Exception
    {
        public VirtualTerminalException(string message)
            : base(message)
        { }

        public VirtualTerminalException(string message, Exception innerException)
            : base(message, innerException)
        { }
    }
}
