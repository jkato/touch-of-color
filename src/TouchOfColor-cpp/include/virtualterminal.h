#pragma once
#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#endif

class VirtualTerminal
{
private:
    enum VirtualTerminalMode
    {
        Disabled,
        Enabled,
    };

public:
    static bool Enable()
    {
        return SetVirtualTerminalMode(VirtualTerminalMode::Enabled);
    }

    static bool Disable()
    {
        return SetVirtualTerminalMode(VirtualTerminalMode::Disabled);
    }

private:
    static bool SetVirtualTerminalMode(const VirtualTerminalMode& mode)
    {
#ifndef _WIN32
        // We're just going to assume that if we're targeting anything non-Windows that
        // we'll support ANSI escape sequences by default.
        return true;
#endif

        HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
        HANDLE hStdIn = GetStdHandle(STD_INPUT_HANDLE);

        if (hStdOut == INVALID_HANDLE_VALUE || hStdIn == INVALID_HANDLE_VALUE)
        {
            return false;
        }

        DWORD outputMode = 0;
        DWORD inputMode = 0;

        GetConsoleMode(hStdOut, &outputMode);
        GetConsoleMode(hStdIn, &inputMode);

        switch (mode)
        {
        case VirtualTerminalMode::Enabled:
        {
            if ((outputMode & ENABLE_VIRTUAL_TERMINAL_PROCESSING) == 0)
            {
                SetConsoleMode(hStdOut, outputMode | ENABLE_VIRTUAL_TERMINAL_PROCESSING);
            }

            if ((inputMode & ENABLE_VIRTUAL_TERMINAL_INPUT) == 0)
            {
                SetConsoleMode(hStdIn, inputMode | ENABLE_VIRTUAL_TERMINAL_INPUT);
            }

            break;
        }
        case VirtualTerminalMode::Disabled:
        {
            break;
        }
        default:
            return false;
        }

        return true;
    }
};
