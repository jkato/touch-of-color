#include <stdio.h>
#include <string>
#include "include/virtualterminal.h"

void PrintTest1();
void PrintTest2();
void PrintTest3();

int main(int argc, char* argv[])
{
    auto enabled = VirtualTerminal::Enable();
    if (!enabled)
    {
        fprintf(stderr, "Failed to enable virtual terminal mode.\n");
        return 1;
    }

    PrintTest1();
    PrintTest2();
    PrintTest3();

    auto disabled = VirtualTerminal::Disable();
    if (!disabled)
    {
        fprintf(stderr, "Failed to disable virtual terminal mode.\n");
        return 1;
    }

    return 0;
}

void PrintTest1()
{
    printf("Test #1:\n");
    for (int i = 0; i < 256; i++)
    {
        if (i % 32 == 0 && i > 0)
        {
            printf("\n");
        }

        printf("\x1b[48;2;255;%d;0m \x1b[0m", i);
    }
    printf("\n");
}

void PrintTest2()
{
    printf("Test #1:\n");
    for (int i = 0; i < 256; i++)
    {
        if (i % 32 == 0 && i > 0)
        {
            printf("\n");
        }

        printf("\x1b[48;2;0;255;%dm \x1b[0m", i);
    }
    printf("\n");
}

void PrintTest3()
{
    printf("Test #3:\n");
    for (int i = 0; i < 256; i++)
    {
        if (i % 32 == 0 && i > 0)
        {
            printf("\n");
        }

        printf("\x1b[48;2;0;%d;255m \x1b[0m", i);
    }
    printf("\n");
}
