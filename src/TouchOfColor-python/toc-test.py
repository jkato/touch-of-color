#!/usr/bin/python
import platform
import sys
from toc import colorize

def main():
    colorize.enable_virtual_terminal()
    print_test_1()
    print_test_2()
    print_test_3()
    colorize.disable_virtual_terminal()

def print_test_1():
    for i in range(0, 256):
        if i % 32 == 0 and i > 0:
            sys.stdout.write("\n")
        sys.stdout.write("\x1b[48;2;255;%d;0m \x1b[0m" % i)
        sys.stdout.flush()
    print("")

def print_test_2():
    for i in range(0, 256):
        if i % 32 == 0 and i > 0:
            sys.stdout.write("\n")
        sys.stdout.write("\x1b[48;2;%d;255;0m \x1b[0m" % i)
        sys.stdout.flush()
    print("")

def print_test_3():
    for i in range(0, 256):
        if i % 32 == 0 and i > 0:
            sys.stdout.write("\n")
        sys.stdout.write("\x1b[48;2;0;255;%dm \x1b[0m" % i)
        sys.stdout.flush()
    print("")

if __name__ == "__main__":
    main()
