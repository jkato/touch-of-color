import platform

STD_OUTPUT_HANDLE = 0xfffffff5
STD_INPUT_HANDLE  = 0xfffffff6

ENABLE_VIRTUAL_TERMINAL_INPUT      = 0x0200
ENABLE_VIRTUAL_TERMINAL_PROCESSING = 0x0004
DISABLE_NEWLINE_AUTO_RETURN        = 0x0008

def enable_virtual_terminal():
    # If we're not running on Windows, then we don't need to do anything.
    if platform.system() != 'Windows':
        return

    # Imports we'll need in order to talk to the Windows API.
    from ctypes import byref, c_uint, windll

    hStdOut = windll.kernel32.GetStdHandle(STD_OUTPUT_HANDLE)

    outputMode = c_uint()
    windll.kernel32.GetConsoleMode(hStdOut, byref(outputMode))

    if (outputMode.value & ENABLE_VIRTUAL_TERMINAL_PROCESSING) == 0:
        windll.kernel32.SetConsoleMode(hStdOut, c_uint(outputMode.value | ENABLE_VIRTUAL_TERMINAL_PROCESSING))

def disable_virtual_terminal():
    # If we're not running on Windows, then we don't need to do anything.
    if platform.system() != 'Windows':
        return

    # Imports we'll need in order to talk to the Windows API.
    from ctypes import byref, c_uint, windll

    hStdOut = windll.kernel32.GetStdHandle(STD_OUTPUT_HANDLE)

    outputMode = c_uint()
    windll.kernel32.GetConsoleMode(hStdOut, byref(outputMode))

    if (outputMode.value & ENABLE_VIRTUAL_TERMINAL_PROCESSING) != 0:
        windll.kernel32.SetConsoleMode(hStdOut, c_uint(outputMode.value & ~ENABLE_VIRTUAL_TERMINAL_PROCESSING))
