# Touch of Color

Libraries, modules, etc. to help with applying colors to console output in various languages and platforms.

## enable-virtual-terminal

enable-virtual-terminal is a small application that can be run in any Windows terminal and will attempt to enable the virtual terminal input and output modes.

**NOTE**: This is only intended to be used on version of Windows that support ANSI escape sequences (Windows 10). It will not enable ANSI escape sequences on versions of windows that do not already support them. If you want/need ANSI escape sequence support on other versions of Windows, you should use a terminal emulator that will support them. ([ConEmu](https://conemu.github.io/) and [hyper.js](https://hyper.is/) to name a few.)

## Windows and ANSI Color Codes

As far as I can tell, and based on some poking around, it doesn't look like cmd.exe on the latest builds of Windows 10 enable the "virtual terminal" input/output modes by default. What this means is that if you want to use ANSI escape sequences in PowerShell or cmd.exe, then you'll just get back your input with some squares with a question mark in them.

More information can found in Microsoft's documentation for [Console Virtual Terminal Sequences](https://docs.microsoft.com/en-us/windows/console/console-virtual-terminal-sequences). But I have provided "enable-virtual-terminal" to preform this task.